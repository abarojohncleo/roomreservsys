<?php

use Illuminate\Database\Seeder;
use App\DepartmentCategory;

class DepartmentCategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DepartmentCategory::create(['name' => 'STEM']);
        DepartmentCategory::create(['name' => 'ABM']);
        DepartmentCategory::create(['name' => 'HUMSS']);
        DepartmentCategory::create(['name' => 'ICT']);
    }
}
