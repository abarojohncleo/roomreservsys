@extends('layouts.app')

@section('title', 'Create Reservation')

@section('add-reservation-form')

	<form action="{{ url('reservations/store') }}" method="post" enctype="multipart/form-data">
		
		<div class="form-group">
			<label>User Name</label>
			<input type="text" name="name" class="form-control" required>
		</div>

		<div class="form--group">
			<label>Room Number</label>
			<input type="text" name="roomnum" class="form-control">
		</div>

		<div class="form--group">
			<label>Purpose</label>
			<input type="text" name="purpose" class="form-control">
		</div>

		<div class="form--group">
			<label>Month</label>
			<input type="text" name="month_usage" class="form-control">
		</div>

		<div class="form--group">
			<label>Date</label>
			<input type="number" name="date_usage" class="form-control">
		</div>

		<div class="form--group">
			<label>Time</label>
			<input type="text" name="time_usage" class="form-control">
		</div>	

		<button type="submit" class="btn btn-success btn-block">Reserved Date</button>

	</form>

@endsection

@section('content')
	
	<div class="container-fluid">
		<div class="row">
			<div class="col-6 mx-auto">
				<h3 class="text-center">Add Reservation</h3>
				<div class="card">
					<div class="card-header">Reservation Information</div>
					<div class="card-body">
						@yield('add-reservation-form')
					</div>
				</div>
			</div>
		</div>
	</div>

@endsection