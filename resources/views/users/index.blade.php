@extends('layouts.app')

@section('title', 'Users')

@section('content')

	<div class="container-fluid">
		<h3>Users</h3>
		@if (!empty (Auth::user()) && Auth::user()->user_role == 'admin')
			<a href="{{ url('users/create') }}" class="create-user" type="button">Create User</a>
		@endif
	</div>
@endsection