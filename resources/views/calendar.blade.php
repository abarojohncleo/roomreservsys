<!DOCTYPE html>
<html>
	<head>
		@extends('layouts.app')
		@section('title', 'Calendar')
		<link rel="stylesheet" type="text/css" href="{{ asset('calendar/style.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ asset('calendar/fontawesome/css/all.min.css') }}">
	</head>
	<body>
		@section('content')
			<div class="container">
				<div class="calendar">
					<div class="month">
					<div><i class="fas fa-arrow-left prev"></i></div>
						<div class="date">
							<h1></h1>
							<p></p>
						</div>
					<div><i class="fas fa-arrow-right next"></i></div>
					</div>
					<div class="weekdays">
						<div style="background-color:red;">Sun</div>
						<div>Mon</div>
						<div>Tue</div>
						<div>Wed</div>
						<div>Thu</div>
						<div>Fri</div>
						<div>Sat</div>
					</div>
					<div class="days mymodal button" id="modalBtn"></div>

					<div id="simpleModal" class="modal">
						<div class="modal-content col-md-8">
							<span id="closeBtn">&times;</span>
							<div class="card">
								<div class="card">
				                    <div class="card-header text-center">Modal Moto</div>
				                    <div class="card-body">
				                    	<p></p>
				                    </div>
				                    <div class="card-footer">
				                    	@if(!empty(Auth::user()) && Auth::user()->user_role == "admin")
					                    	<a class="btn btn-primary" href="{{url('reservations/create')}}">Add Reservation</a>
					                    @endif
				                    </div>
				                </div>
							</div>
						</div>
					</div>
				</div>
			</div>
		@endsection

		<script type="text/javascript" src="{{ asset('calendar/calendar.js') }}"defer></script>
	</body>
</html>
