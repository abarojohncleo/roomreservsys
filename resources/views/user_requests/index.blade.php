@extends('layouts.app')

@section('title', 'Users Request')

@section('content')
	
	<div class="container-fluid">
		<h3>My Requests</h3>

		@if (!empty(Auth::user()) && Auth::user()->user_role != 'admin')
			<a href="" class="btn btn-primary">Create Request</a>
		@endif
	</div>

@endsection