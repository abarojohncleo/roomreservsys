<nav class="navbar navbar-expand-lg navbar-dark bg-dark">

	<a class="navbar-brand" href="">
		<img src="/img/feu.png" width="30" height="30" class="d-inline-block align-top" alt="" loading="lazy">
		{{ config('app.name', 'RoomResrv') }}
	</a>

	<button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbar">
		<span class="navbar-toggler-icon"></span>
	</button>

	<div class="collapse navbar-collapse text-right" id="navbar">
		<ul class="navbar-nav mr-auto">
			<li class="nav-item">
				<a class="nav-link" href="{{url('/')}}">Calendar</a>
			</li>
		</ul>

		<ul class="navbar-nav ml-auto">
			@guest
				<li class="nav-item">
					<a href="{{url('/login')}}" class="nav-link">Login</a>
				</li>

				<li class="nav-item">
					<a href="{{url('/register')}}" class="nav-link">Register</a>
				</li>
			@else
				@if(!empty(Auth::user()) && Auth::user()->user_role == 'admin')

					<li class="nav-item">
						<a href="{{url('/users')}}" class="nav-link">Users</a>
					</li>

					<li class="nav-item">
						<a href="{{url('/requests')}}" class="nav-link">Request</a>
					</li>

				@else

					<li class="nav-item">
						<a href="{{url('/user_requests')}}" class="nav-link">Request</a>
					</li>

				@endif

				<li class="nav-item">
					<a onclick="document.querySelector('#logout-form').submit()" href="#" class="nav-link">Logout</a>
				</li>
				
			@endguest
		</ul>
	</div>

</nav>

<form id="logout-form" class="d-none" action="{{route('logout')}}" method="POST">
	@csrf
</form>