<?php

Auth::routes();

Route::get('/', function () {
    return view('calendar');
});

Route::get('/reservations', 'RoomReservation@index');
Route::get('/reservations/create', 'RoomReservation@create');

Route::get('/user_requests', 'UserRequestController@index');



Route::get('users','UserController@index');
Route::get('/users/create','UserController@create');

