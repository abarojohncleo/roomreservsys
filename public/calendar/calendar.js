// getting calendar month
const date = new Date();

const renderCalendar = () => {

	date.setDate(1);

	var lastDay = new Date(date.getFullYear(),date.getMonth()+1,0 ).getDate(); // getting the last date of the current month

	var firstDayIndex = date.getDay();

	var prevLastDay = new Date(date.getFullYear(),date.getMonth()+1, 0 ).getDate();

	var lastDayIndex = new Date(date.getFullYear(),date.getMonth()+1,0 ).getDay();

	var nextDays = 7 - lastDayIndex - 1; 

	var monthDays = document.querySelector(".days");

	var monthNames = [
		"January", 
		"February", 
		"March", 
		"April", 
		"May", 
		"June",
	  	"July", 
	  	"August", 
	  	"September", 
	  	"October", 
	  	"November", 
	  	"December"
	];



	document.querySelector(".month h1").innerHTML = monthNames[date.getMonth()];  //month
	document.querySelector(".date p").innerHTML = new Date().toDateString();  //date

	// putting up the dates from 1-31;
	let days = [""];

	//setting up the previous dates
	for (let x = firstDayIndex; x > 0; x--) {
		days+= `<div class="prev-date">${prevLastDay - x+1 }</div>`;
	}

	//putting up how many days in accordance with the dates present in a month
	for (let i = 1; i <= lastDay; i++) {
		if (i === new Date().getDate() && date.getMonth() === new Date().getMonth()){
			days += `<div class="today">${i}</div>`;
		} else {
			days+= `<div>${i}</div>`;
		}
	}

	for (let j =1; j<= nextDays; j++) {
		days+= `<div class="next-date">${j}</div>`;
		monthDays.innerHTML = days;
	}

}

document.querySelector('.prev').addEventListener('click', () => {
	date.setMonth(date.getMonth()-1);
	renderCalendar();
});

document.querySelector('.next').addEventListener('click', () => {
	date.setMonth(date.getMonth()+1);
	renderCalendar();
});

renderCalendar();


// for modal structure

// getting elements
var modal = document.getElementById('simpleModal');
var modalBtn = document.getElementById('modalBtn');
var closeBtn = document.getElementById('closeBtn');

// listem event
modalBtn.addEventListener('click', openModal);
closeBtn.addEventListener('click', closeModal);
window.addEventListener('click', clickOutside);

// function 
function openModal(){
	modal.style.display = 'block';
}
function closeModal(){
	modal.style.display = 'none';
}
function clickOutside(e){
	if (e.target == modal) {
		modal.style.display = 'none';
	}
}

